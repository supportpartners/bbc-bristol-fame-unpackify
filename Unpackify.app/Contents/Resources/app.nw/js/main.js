var appName = "Unpackify";

var strFileList;
var arrReels = [];
var arrReelsFullPath = [];
var strJobName = '';
var strFileListRoot = '';
var fs = require('fs');
var fuj = true;

var strJobFolderParent = '';
var strNewJobFolderParent = '';
var unpackDest = '';
var newDestinationPicked = false;


var mo = require('moment');
mo = mo().format('YYMMDD');

var logFile = "/Users/Shared/Logs/Unpackify/log_" + mo + ".txt";
var logParent = "/Users/Shared/Logs/";

var reportingDir = "/Volumes/BPVMCR/07_FPL_PROCESSING/02_FPL_POC_FILES/01_JSON/"

var verboseLogging = true;

document.onreadystatechange = function () {
  var state = document.readyState;
  if (state == 'interactive') {
     // init();
  } else if (state == 'complete') {
  
	  initOnCompleteLoad();
	  
	  writeLogToScreen("Welcome to Unpackify!", 0);
	  
  }
}

	function writeLogToScreen(strMessage, level){
		try{
			if (verboseLogging == true || level == 1){
				var now = require('moment');
				now = now().format('YY/MM/DD HH:mm:ss');
			
				document.getElementById('appMessages').innerHTML = strMessage;
				writeLog(now + ": " + strMessage + "\n");
			}
		}catch(e){
			document.getElementById('appMessages').innerHTML = "Logging error: " + e.message;
		}
	}
	
	function writeLog(strMessage){
		if (!fs.existsSync(logParent)) {	
			fs.mkdirSync(logParent, err => {
    			if (err && err.code != 'EEXIST') throw err	
			});
		}
		
	  	if (!fs.existsSync(logParent + appName + "/")) {	
			fs.mkdirSync(logParent + appName + "/", err => {
    			if (err && err.code != 'EEXIST') throw err	
			});
		}
	
		//fs.appendFile(logFile, strMessage, function (err) {
  		//	if (err) throw err;
		//});
		
		fs.appendFileSync(logFile, strMessage);
	}


function initOnCompleteLoad(){
	

	document.getElementById("open").addEventListener('change', function (e) {
		writeLogToScreen("User selected a new job folder", 0);
		
		strFileList = e.target.files;
		
		if (strFileList.length > 0){
			writeLogToScreen(strFileList.length + " files found within that structure", 0);
			//if (strFileList[0].name.toLowerCase() != '.ds_store'){
				//writeLogToScreen("");
				//strFileList[i].name = name;
				//strFileList[0].webkitRelativePath = path after the selected directory name
				var strFolderPath = this.value.split(";")[0];
				var strFileRelPath = '';
				
				for (var i=0; i < strFileList.length; i++){
					if (!strFileList[i].name.startsWith(".")){
						strFileRelPath = strFileList[i].webkitRelativePath;
						writeLogToScreen("Relative file path to find job name from: " + strFileRelPath, 0);
						strFolderPath = this.value.split(";")[i];
						writeLogToScreen("Folder path: " + strFolderPath, 0);
						i = strFileList.length + 10;
					}
				}
				
				if (strFileList != ""){
					//strJobName = strFolderPath.replace(strFileRelPath, '');
					var folderNameSplit = strFileRelPath.split("/");
					strJobName = folderNameSplit[0];
					
					writeLogToScreen("Job name: " + strJobName, 0);
				
					var fileListRootTemp = strFolderPath.replace(strFileRelPath, '');
					unpackDest = fileListRootTemp + strJobName;
					strJobFolderParent = unpackDest;
					
					writeLogToScreen("Unpack files to: " + unpackDest, 0);
					
					populateJobName();
					
					writeLogToScreen("Initial stage complete.", 0);
				}else{
					writeLogToScreen("Only files beginning with '.' found. Please check directory and try again", 1);
				}
								
			//}else{
			//	writeLogToScreen("No files found, check directory and try again");
			//}
		}else{
			writeLogToScreen("No files found, check directory and try again", 1);
		}
		
	});
}



function populateJobName(){
	document.getElementById('dStep2').style = "visibility: visible";

	document.getElementById('jobName').innerHTML = strJobName;
	
	processingJobName = strJobName;
	
	writeLogToScreen("Getting the reels from the job folder", 0);
	getReels();
}

function getReels(){
	
	
	var filePath = '';
	var ul = document.getElementById("reels");
	ul.innerHTML = "";
	writeLogToScreen("Cleared reel display value from screen", 0);
	
	for (var i=0; i < strFileList.length; i++){
		for (var i=0; i < strFileList.length; i++){
			writeLogToScreen("Checking if filename starts with a . and if so, ignore", 0);
			if (!strFileList[i].name.startsWith(".")){
			
				filePath = strFileList[i].webkitRelativePath;
				
				var filePathSplit = filePath.split("/");
				var reelName = filePathSplit[1];
				var reelPath = filePathSplit[0] + '/' + filePathSplit[1];
				
				writeLogToScreen("Check if reel already added to array, if so, ignore: " + reelName, 0);
				if (arrReels.indexOf(reelName) < 0){
					writeLogToScreen("Adding reel full path to array: " + reelPath, 0);
					arrReelsFullPath.push(reelPath);
				
					let item = document.createElement("li");
					item.innerHTML = reelName;
					writeLogToScreen("Adding reel name to display: " + reelName, 0);
					document.getElementById("reels").appendChild(item);
					writeLogToScreen("Adding reel name to reel array: " + reelName, 0);
					arrReels.push(reelName);
				}
			}
		}
	}
	
	document.getElementById('pLocation').innerHTML = "Unpacking to: " + unpackDest + " if you want to unpack to a different destination, enter the new path below (this must be an existing location):";
}



function doUnPack(){
	var blnContinue = false;

	try{
		writeLogToScreen("Checking if new destination entered", 0);
		var newDest = document.getElementById('txtDestination').value;
		
		writeLogToScreen(newDest + " for new destination", 0);
		
		if (newDest != ""){
			writeLogToScreen("Check if " + newDest + " exists", 0);
			if (fs.existsSync(newDest)) {
				writeLogToScreen(newDest + " exists", 0);
				
				unpackDest = newDest + "/" + strJobName;
				
				if (!fs.existsSync(unpackDest)) {
					fs.mkdirSync(unpackDest, err => {
    					if (err && err.code != 'EEXIST') throw err	
					});
				}
				
				newDestinationPicked = true;
				
				blnContinue = true;
			}else{
				writeLogToScreen("Path " + newDest + " does not exist. Please correct, or remove it from the text box below", 1);
			}
		}else{
			blnContinue = true;
		}
	}catch(e){
		writeLogToScreen("Error when checking the new destination: " + e.message, 1);
		blnContinue = false;
	}
	
	writeLogToScreen("Continue? " + blnContinue, 0);
	try{
		if (blnContinue == true){
			writeLogToScreen("Writing unpacked files out to: " + unpackDest, 0);
	
			try{
				writeLogToScreen("Check if file list contains files", 0);
				if (strFileList != undefined){
					writeLogToScreen(strFileList, 0);
					if (strFileList.length > 0){
						writeLogToScreen(strFileList.length, 0);
						var unpack = require('unpack.js');
						writeLogToScreen("Handing off to the unpacking module", 0);
						
						unpack.startUnpacking(document, strJobName, strFileList, arrReels, strFileListRoot, unpackDest, strJobFolderParent, verboseLogging, logFile, newDestinationPicked);
						
					}else{
						writeLogToScreen('Please select a folder before continuing', 1);	
					}
				}else{
					writeLogToScreen('Please select a folder before continuing', 1);	
				}
			}catch(e){
				writeLogToScreen("Caught error: " + e.message, 1);
			}
		}
	}catch(e){
		writeLogToScreen("Error caught when trying to unpack: " + e.message, 1);
	}
	
	//writeLogToScreen("Ending", 0);
}