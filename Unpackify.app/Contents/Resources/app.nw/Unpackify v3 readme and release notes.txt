Readme
======

App written in NodeJS


Variables:
-----------
Paths are stored in the 'main.js' file located in the js folder when you show contents -> Unpackify.app/Contents/Resources/app.nw/js/main.js

These are the ones that most likely need to be changed:

1. var reportingDir = "/Users/Shared/Reporting/"
- This is the location of where the Reporting files go to. This Reporting is for the JSON files to be used in future development for dashboard reporting on the status of jobs. You get one JSON file per Job.

2. var logFile = "/Users/Shared/Logs/" + appName + "/log_" + mo + ".txt"
- This is the log file :) If you need to change this, make sure to only change the bit at the beginning (before " + appName) only.

2. var logParent = "/Users/Shared/Logs/"
- This is the log file parent directory :) If it doesn't exist it will be auto created.

4. var verboseLogging = true
- This outputs a lot of info to the log file and the screen. Probably useful, but included as an option if you want less noise


There is also the following in unpack.js - found in Unpackify.app/Contents/Resources/app.nw/node_modules/unpack.js
1. var reportingDir = "/Users/Shared/Reporting/"
- This is the location of where the Reporting files go to. This Reporting is for the JSON files to be used in future development for dashboard reporting on the status of jobs. You get one JSON file per Job - set this the same as in the main.js


What does Unpackify do?!
----------------------

User selects the ‘Job’ folder. This is the parent contains the reels that need to be unpacked. 
The user can unpack them to a new location (by writing the full path in to the box provided)
Start Unpacking will create the unpacked and working directories and then move all media in to the correct place. All remaining files will be put into a 'husks' folder within working.

Problems with the app starting and then crashing? Kill the directory in ~/Library/Application Support and reopen the app. It should clear it :)


Version release notes:
v1 September, written by Jemma
v2 18 November, fixed RDC file names
v3 25 November, removed SOURCE folder, added styling to appmessage to make it bigger, shortened RDC names after feedback from DV, and shortened file names for R3D and F55 MXFs which use shorter naming convention, by not repeating date or adding camera format (using conditions in unpack.js)


